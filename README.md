# screenFOOD - Container as a Service - CMS 
## General Available (-ga) Release for Version 12.0.1
 
### 1. Image Content / docker-compose.yaml Content
This compose file contains as default four services. We follow the convention to label the service and the container name the same. 
The following list shows all services and the startup order 

1. sfn-cms-db | PostgresSQL DBRMS image/container 

2. sfn-cms | screenFOODnet screenFOOD CMS Server Applikation image/container 

3. sfn-cms-player | screenFOODnet screenFOOD CMS Player Applikation image/container 

4. sfn-cms-proxy | ngninx high performance web server used as reverse proxy image/container 

### 2. Folder Dependencies 
In this version we have some folder depencies as we are using binded volumes to the host for better administration. So there is no need to access containers OS directly to change configuration, see the media files or the backups. Escpecially the backup folder is a good thing, so customers backup solution can then directly fetch this folder to backup. 
The negative aspekt to binded volumes is, they have to be present, means, they have to be created on host file system in prior to run the containers. 
For that we have prepared a POSH script to create those folders. But feel free to create your folders by hand using your OS's file system explorer e.g. Windows Explorer, you might want to refer to the <folders> file within this repo. There you have all folder, with their path. 
 
### 3. Folder Description 
The following table shows what's the content of each folder is: 

| Folder name        | Description           | Container mapping  |
| ------------- |:-------------:| -----:|
| sf_backup      | This folder contains the DBRMS SQL dumps for backups | sfn-cms:/opt/sf_backup |
| properties      | This folder contains the screenFOODnet specific application server configuration file for Apache Tomcat server and sub-applications    |  sfn-cms:/opt/tomcat/properties |
| conf | This folder contains the Apache Tomcat server default configuration folder, like server.xml users.xml etc.      |  sfn-cms:/opt/tomcat/conf |
| logs | This folder contains the Apache Tomcat server and screenFOOD Application logs, like catalina.out, sf_app.log etc.      |  sfn-cms:/opt/tomcat/logs |
| media | This folder contains all media files uploaded to the screenFOOD CMS server     |  sfn-cms:/opt/tomcat/webapps/ROOT/sf/new/media/ |
| nginxlogs | This folder contains all nginx web server service logs     |  sfn-cms-proxy:/var/logs |
| nginxconf | This folder contains all nginx web server configuration files    |  sfn-cms-proxy:/etc/nginx/conf.d/ |
| sslcerts | This folder contains all nginx web server ssl certificates, and could be used to upload private CA certs for binding in nginx configuration <screenfood.conf>    |  sfn-cms-proxy:/etc/nginx/ssl/letsencrypt/certificates/ | 

### 4. Environment Exectution 
In this section we're going to describe how to bring the whole environment up with some tiny steps, assuming you have Docker Desktop installed: 
*please note we're describing for MS Windows host* 

1. Create folders on hosts file system using the create-folder.ps1 POSH script or by using Windows Explorer (don't forget to create the symlink too) (Default path is: C:\sfn-cms\<folder>). 

2. Change your current working directory to the one where our docker-compose.yaml file is located 

3. Run the following command: 

```Powershell
$> docker-compose up -d
``` 
 
*That's it. You can browse to https://localhost or https://your-server-fqdn. Note that, while deploying and initialization of the environment it might take up to five minutes to get the server ready, but your browser should wait for the sever to load the application, so keep calm and enjoy!* 