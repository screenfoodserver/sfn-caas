function Add-FolderOn([string]$Driveletter){
    try { $folder = Get-Content -Path "$PWD\folders" -ErrorAction Stop; }
    catch {
        write-host -ForegroundColor red "Can't find <folders> file, seems that is not in the same folder as this script.";
    } 
    if($folder.Length -eq 0){
        write-host -ForegroundColor yellow "<folders> files seems to be there but it's empty, do you have the right file?";
    }
    else{
        $folder | ForEach-Object {
            $WinDriveFull = $Driveletter + ":\";
            $path = $WinDriveFull + (($_).replace("/","\"));
            $chk = Test-Path -Path $path
            if($chk -eq $false){ 
                New-Item -Path $path -Type Directory;
                Write-Host -ForegroundColor green "<$path> created."
                if($_ -eq "sfn-cms/properties"){
                    $Junc = $WinDriveFull + "sfn-cms\player";
                    & cmd /c mklink /J $Junc $path
                }
            }
            else {
                write-host -ForegroundColor blue "<$path> already exist, skiping."
            }
        }
    }
}Add-FolderOn("C"); ### change letter

function Remove-FolderOn([string]$Driveletter) {
    try { $folder = Get-Content -Path "$PWD\folders" -ErrorAction Stop; }
    catch {
        write-host -ForegroundColor red "Can't find <folders> file, seems that is not in the same folder as this script.";
    } 
    if($folder.Length -eq 0){
        write-host -ForegroundColor yellow "<folders> files seems to be there but it's empty, do you have the right file?";
    }
    else{
        $folder | ForEach-Object {
            $WinDriveFull = "C:\"; #$Driveletter + ":\";
            $folder | ForEach-Object {
                $path = $WinDriveFull #+ (($_).replace("/","\"))
                try { 
                    Remove-Item -Recurse -Path $path -Force -ErrorAction Stop;
                    Write-Host -ForegroundColor green "<$path> removed sucessfully."
                }
                catch {
                    Write-Host -ForegroundColor red "<$path> throws an excpetion while trying to remove. Folder still there."
                }
            }
        }
    }
}

